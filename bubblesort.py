# This is just a simple sort end wala element at the end jake sort ho jata h automatic so no problem.
# comparision hi karne h
# This is A STABLE SORT
# ye swapped wala varible best case me complexity o(n) deta h baki sare cases ke liye o(n^2) h
# just remember ye comparision wali algorithm h



def bubblesort(arr):
    i = 0
    swapped = True
    while i < len(arr) and swapped:
        j = 0
        swapped = False
        while j < len(arr) - i - 1:
            if arr[j] > arr[j + 1]:
                temp = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
                swapped = True
            j += 1
        i += 1


a = [9, 7, 6, 15, 16, 5, 10, 11]

bubblesort(a)
print(a)
