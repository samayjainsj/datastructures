# Counting Sort implementation in python

# def countingsort(arr):
#     count = []
#     range_arr = []
#     i = 0
#     j = 0
#     min = arr[0]
#     max = arr[0]
#     while i < len(arr):
#         if min > arr[i]:
#             min = arr[i]
#         if max < arr[i]:
#             max = arr[i]
#         i += 1
#     i = min
#     while i <= max:
#         range_arr.append(i)
#         i += 1
#     j = 0
#     while j < len(range_arr):
#         count.append(0)
#         j += 1
#     i = 0
#     while i < len(arr):
#         count[arr[i] - min] += 1
#         i += 1
#     i = 1
#     while i < len(range_arr):
#         count[i] = count[i] + count[i - 1]
#         i += 1
#     ans = [0]*len(arr)
#     i = 0
#     while i < len(arr):
#         index = count[arr[i] - min] - 1
#         ans[index] = arr[i]
#         i += 1
#     return ans
#
#
# a = [1, 4, 1, 2, 7, 5, 2]
# print(countingsort(a))


def counting_sort(array, maxval):
    """in-place counting sort"""
    n = len(array)
    m = maxval + 1
    count = [0] * m               # init with zeros
    for a in array:
        count[a] += 1             # count occurences
    i = 0
    for a in range(m):            # emit
        for c in range(count[a]): # - emit 'count[a]' copies of 'a'
            array[i] = a
            i += 1
    return array

print counting_sort( [1, 4, 7, 2, 1, 3, 2, 1, 4, 2, 3, 2, 1], 7 )