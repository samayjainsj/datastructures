# just remember this thing ki quicksort ka compleminet h
# usme pivot element return hota h isme middle wala element hota h

def mergesort(arr):
    if len(arr) == 0 or len(arr) == 1:
        return arr
    else:
        mid = int(len(arr)/ 2)
        a = mergesort(arr[:mid])
        b = mergesort(arr[mid:])
        return merge(a,b)


def merge(a,b):
    c = []
    i = 0
    j = 0
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            c.append(a[i])
            i += 1
        else:
            c.append(b[j])
            j += 1
    while i < len(a):
        c.append(a[i])
        i += 1
    while j < len(b):
        c.append(b[j])
        j += 1
    return c


a = [9, 7, 6, 15, 16, 5, 10, 11]
print(mergesort(a))