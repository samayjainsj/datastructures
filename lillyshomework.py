def selectionsort(arr):
    i = 0
    swap = 0
    while i < len(arr):
        min = i
        j = i+1
        while j < len(arr):
            if arr[j] < arr[min]:
                min = j
            j += 1
        if min != i:
            temp = arr[min]
            arr[min] = arr[i]
            arr[i] = temp
            swap += 1
        i += 1
    return swap

length = input()
arr = [int(i) for i in input().split()]
print(selectionsort(arr))