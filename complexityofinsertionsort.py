def insertionsort(arr):
    i = 0
    count = 0
    while i < len(arr) - 1:
        v = arr[i + 1]
        j = i + 1
        while j >= 1 and arr[j - 1] > v:
            arr[j] = arr[j - 1]
            count += 1
            j -= 1
        arr[j] = v
        i += 1
    return count


length = int(input())
arr = [int(i) for i in input().split()]
print(insertionsort(arr))
