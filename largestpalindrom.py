def checkpalindrom(num):
    reverse_num = 0
    temp = num;
    while temp != 0:
        remainder = temp % 10
        reverse_num = reverse_num * 10 + remainder
        temp /= 10
    if reverse_num == num:
        return 1
    else:
        return 0


n = 999
i = n
arr = []
while i >= 100:
    j = n
    while j >= 100:
        arr.append(i*j)
        # if checkpalindrom(i * j) == 1:
        #     print(i * j)
            # break
        j -= 1
    i -= 1
arr.sort(reverse=True)
for i in arr:
    if checkpalindrom(i) == 1:
        print(i)
        break