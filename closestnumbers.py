def mergesort(arr):
    if len(arr) == 0 or len(arr) == 1:
        return arr
    else:
        mid = int(len(arr) / 2)
        a = mergesort(arr[:mid])
        b = mergesort(arr[mid:])
        return merge(a, b)


def merge(a, b):
    c = []
    i = 0
    j = 0
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            c.append(a[i])
            i += 1
        else:
            c.append(b[j])
            j += 1
    while i < len(a):
        c.append(a[i])
        i += 1
    while j < len(b):
        c.append(b[j])
        j += 1
    return c


length = input()
a = [int(i) for i in input().split()]
x = []
arr = mergesort(a)
i = 0
diff = arr[1] - arr[0]
while i < len(arr) - 1:
    if diff > arr[i + 1] - arr[i]:
        x = []
        x.append(arr[i])
        x.append(arr[i+1])
        diff = arr[i+1] - arr[i]
    elif diff == arr[i+1] - arr[i]:
        x.append(arr[i])
        x.append(arr[i+1])
    i += 1

print(" ".join(str(ele) for ele in x))
