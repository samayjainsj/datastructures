arr = []

def primechecking(arr,n):
    count = 0
    i = 2
    while i<int(n/2):
        if n%i == 0:
            count += 1
        i += 1
    if count == 0:
        arr.append(n)

j = 2
while len(arr) < 10001:
    primechecking(arr,j)

print(arr)