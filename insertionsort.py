# In this algorithm we insert the element we just simply make a window of sorted array
# and then insert it 2 4 bar try karna ho jayega chill bro
# This is A STABLE SORT ALGORITHM


def insertionsort(arr):
    i = 0
    while i < len(arr)-1:
        temp = arr[i+1]
        j = i+1
        while j >= 1 and temp < arr[j-1]:
            arr[j] = arr[j-1]
            j -= 1
        arr[j] = temp
        i += 1


a = [9, 7, 6, 15, 16, 5, 10, 11]
insertionsort(a)
print (a)
