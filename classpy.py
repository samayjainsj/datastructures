class Employee:
    'Common base class for all employees'
    deco = 5
    def myfun(self):
        deco1 = self.deco
        print deco1+3

x = Employee()
y = Employee()
y.deco = 7
x.myfun()
y.myfun()