def insertionsort(arr):
    i = 0
    while i < len(arr) - 1:
        temp = arr[i + 1]
        j = i + 1
        while j >= 1 and temp < arr[j - 1]:
            arr[j] = arr[j - 1]
            j -= 1
        arr[j] = temp
        i += 1
        print(" ".join(str(x) for x in arr))


length = input()
arr = [int(i) for i in input().split()]
insertionsort(arr)