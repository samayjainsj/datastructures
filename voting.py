# just remember this thing ki quicksort ka compleminet h
# usme pivot element return hota h isme middle wala element hota h

def mergesort(arr):
    if len(arr) == 0 or len(arr) == 1:
        return arr
    else:
        mid = int(len(arr) / 2)
        a = mergesort(arr[:mid])
        b = mergesort(arr[mid:])
        return merge(a, b)


def merge(a, b):
    c = []
    i = 0
    j = 0
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            c.append(a[i])
            i += 1
        else:
            c.append(b[j])
            j += 1
    while i < len(a):
        c.append(a[i])
        i += 1
    while j < len(b):
        c.append(b[j])
        j += 1
    return c


a = [1, 3, 1, 2, 2, 1, 2, 1, 3, 4, 3, 4, 4, 4, 4, 4, 3, 2]
a = mergesort(a)

current = a[0]
prev_count = 0
curr_count = 1
i = 1
while i < len(a):
    if current == a[i]:
        curr_count += 1
    else:
        if curr_count > prev_count:
            max_vote = curr_count
            won = current
        else:
            max_vote = prev_count
            won = prev
        prev_count = max_vote
        curr_count = 1
        prev = won
        current = a[i]
    i += 1
if curr_count > prev_count:
    max_vote = curr_count
    won = current
else:
    max_vote = prev_count
    won = prev
print("Winner is %d and won by %d votes" % (won, max_vote))
